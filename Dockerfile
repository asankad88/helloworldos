ARG GO_VERSION=1.14


FROM golang:${GO_VERSION}-alpine AS builder
RUN apk update && apk add alpine-sdk git && rm -rf /var/cache/apk/*

RUN mkdir -p /api
WORKDIR /api
COPY go.mod .
COPY . .
RUN make build

FROM alpine:latest
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

RUN mkdir -p /api
WORKDIR /api
COPY --from=builder /api/bin/hello-server .

EXPOSE 8090

ENTRYPOINT ["./hello-server"]
