PROJ=hello-server
ORG_PATH=akoya.com
REPO_PATH=$(ORG_PATH)/$(PROJ)
export PATH := $(PWD)/bin:$(PATH)
export GOBIN=$(PWD)/bin
$( shell mkdir -p bin )
clean:
	@rm -rf bin/

build:
	@CGO_ENABLED=1 go build -o ./bin/$(PROJ) -gcflags "-N -l" -v  $(REPO_PATH)
	@go test -coverprofile cov.out -coverpkg ./... ./...
	@go test -json > report.json ./... ./...
install:
	@CGO_ENABLED=1 go install -o ./bin/$(PROJ) -gcflags "-N -l" -v  $(REPO_PATH) -o $(PWD)/bin
